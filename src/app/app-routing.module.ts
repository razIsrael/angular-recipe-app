import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecipesComponent } from './recipes/recipes.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeStartComponent } from './recipes/recipe-start/recipe-start.component';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { recipesResolverService } from './recipes/recipes-resolver.service';
import { AuthComponent } from './auth/auth.component';

const appRoutes: Routes=[
  { path: '',   redirectTo: '/recipe', pathMatch: 'full' },
     { path: 'recipe', component: RecipesComponent, 
     children: [
        {path: '', component: RecipeStartComponent},
        {path: 'new', component: RecipeEditComponent},
        {
          path: ':id', 
          component: RecipeDetailComponent, 
          resolve: [recipesResolverService]
        },
        {
          path: ':id/edit', 
          component: RecipeEditComponent,
          resolve: [recipesResolverService]
        }
    ] },
   
     { path: 'shoppingList', component: ShoppingListComponent },
     { path: 'auth', component: AuthComponent }    
     ];

@NgModule({
    imports: [
        RouterModule.forRoot(
          appRoutes,
          { enableTracing: true } // <-- debugging purposes only
        )
      ],
      exports: [
        RouterModule
      ]
})
export class appRoutingModule {

}
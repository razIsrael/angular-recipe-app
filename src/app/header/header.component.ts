import { Component, OnInit, EventEmitter, Input, Output, OnDestroy } from '@angular/core';
import { DataStorageService } from '../shared/data-storage.service';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit,OnDestroy {
  private userSub: Subscription;
  collapsed = true;
  isAuthenticated = false;
  
  constructor(private dataStorageService: DataStorageService,
      private authService: AuthService) { }
  
      ngOnInit() {
    this.userSub = this.authService.user.subscribe(user => {
      this.isAuthenticated = !!user;
      //this.isAuthenticated = !user ? false: true;
    });
 }
 
 onSaveData(){
   this.dataStorageService.storeRecipes();
 }
 onFetchData(){
   this.dataStorageService.FetchRecipes().subscribe();
 }
 onLogout(){
   this.authService.logout();
 }
  //  swichToRecipe(Achoce:boolean = true){
  //    this.Chose.emit(Achoce)}

  //  swichToSopping(Achoce:boolean = false){
  //   this.Chose.emit(Achoce)
  //   //  console.log(Achoce);   }

  ngOnDestroy(){
    this.userSub.unsubscribe();
  }

}

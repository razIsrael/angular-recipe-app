import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { RecipeService } from '../recipe.service';
import { Recipe } from '../recipe.model';
import { relative } from 'path';

@Component({
  selector: 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls: ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {
  id: number;
  editMode = false;
  recipeForm: FormGroup;
  constructor(private route: ActivatedRoute,
              private recipeService: RecipeService,
              private router: Router) { }

  ngOnInit() {
    this.route.params.
    subscribe(
      (params: Params) => {
        this.id = +params['id'];//from the rout setup
        this.editMode = params['id'] != null; //if it exist = false, else true
        this.inItForm();
        // console.log(this.editMode);
      }
    );
  }
  onAddIngredient(){
    (<FormArray>this.recipeForm.get("ingredients")).push(
      new FormGroup({
        'name': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ])
      })
    );
  }

  onCancel(){
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  private inItForm(){
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDiscription = '';
    let recipeIngredients = new FormArray([]);

    if(this.editMode) {//we are in edit mode so get frome here
      const recipe = this.recipeService.getRecipe(this.id);
      recipeName = recipe.name;
      recipeImagePath = recipe.imagePath;
      recipeDiscription = recipe.descciption;
      if(recipe['ingredients']) {
        for (let ingeident of recipe.ingredients){
          recipeIngredients.push(
            new FormGroup({
              'name': new FormControl(ingeident.name, Validators.required),
              'amount': new FormControl(ingeident.amount,[
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/)
              ])
            })
          );
        }

      }
    }

    this.recipeForm = new FormGroup({
      'name': new FormControl(recipeName, Validators.required),
      'imagePath': new FormControl(recipeImagePath, Validators.required),
      'description': new FormControl(recipeDiscription, Validators.required),
      'ingredients': recipeIngredients
    });
    
    
  }
 
  get controls() { // a getter!
    return (<FormArray>this.recipeForm.get('ingredients')).controls;
  }
 
  onSubmit(){
    const newRecipe = new Recipe(
      this.recipeForm.value['name'],
      this.recipeForm.value['description'],
      this.recipeForm.value['imagePath'],
      this.recipeForm.value['ingredients']  );

      //or this.recipeForm.value

    if(this.editMode){
      this.recipeService.updateRecipe(this.id, newRecipe)
    }else{
      this.recipeService.addRecipe(newRecipe);
    }
    // console.log(this.recipeForm);
    this.onCancel();
  }
  onDeleteIngredient(index: number){
    (<FormArray>this.recipeForm.get('ingredients')).removeAt(index);
  }

}

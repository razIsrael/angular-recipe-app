import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';

import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  // @Output() RecipeSelected = new EventEmitter<Recipe>();
  recipes: Recipe[];
  subscription: Subscription;

  constructor(private recipeService: RecipeService,
    private router: Router,
    private rout: ActivatedRoute) { }

  ngOnInit() {
    this.subscription =  this.recipeService.recipeChanged
    .subscribe(
      (recipes: Recipe[]) => {this.recipes = recipes}
    );
    this.recipes = this.recipeService.getRecipes(); 
  }
  onNewRecipe(){
    this.router.navigate(['new'],{relativeTo: this.rout});
  }

  // onRecipeSelected(recipe: Recipe){
  //   this.RecipeSelected.emit(recipe);
  // }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}

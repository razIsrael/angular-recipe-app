import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { shoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';
// import { Subject } from 'rxjs';

@Injectable()
export class RecipeService {
   recipeChanged = new Subject<Recipe[]>();
   //  recipeSelected = new EventEmitter<Recipe>();
   // recipeSelected = new Subject<Recipe>();

   //  private recipes:Recipe[] = [
   //      new Recipe('a test recipr',
   //      'taste taco!',
   //      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSMFjb1XT6W6Otu2UcCFhaUnAVvqOnG4EFySFmH4vlF3S00s3mbPA&s',
   //      [new Ingredient('meat',2),new Ingredient('Fries',20)]),
   //      new Recipe('a test 2',
   //      'taste chicke wing',
   //      'https://static01.nyt.com/images/2019/12/11/dining/11as-pasta-with-ricotta-and-lemon/merlin_155855151_a42a72df-8230-420d-b9b9-d81fe089ff55-articleLarge.jpg',
   //      [new Ingredient('meat',5),new Ingredient('Fries',50)]),
   //      new Recipe('a test recipr',
   //      'taste chiken!',
   //      'https://www.rockrecipes.com/wp-content/uploads/2009/02/Crispy-Honey-Garlic-Chicken-Wings-1.jpg',
   //      [])
   //   ];
   private recipes: Recipe[] = [];

     constructor(private slService: shoppingListService){}

     setRecipes(recipes: Recipe[]){
        this.recipes = recipes;
        this.recipeChanged.next(this.recipes.slice());
     }
     getRecipe(index: number){
        return this.recipes[index];
     }

     getRecipes(){
         return this.recipes.slice();
     }
     addIngredientsToShoppingList(ingredients: Ingredient[]){
        this.slService.addIngredients(ingredients);
     }

     addRecipe(recipe: Recipe){
        this.recipes.push(recipe);
        this.recipeChanged.next(this.recipes.slice());
     }

     updateRecipe(index: number,newRecipe: Recipe){
        this.recipes[index] = newRecipe;
        this.recipeChanged.next(this.recipes.slice());
     }

     deleteRecipe(index:number){
        this.recipes.splice(index,1);
        this.recipeChanged.next(this.recipes.slice());

     }
}
import { Directive, HostListener, ElementRef, OnInit, Renderer2, HostBinding } from "@angular/core";

@Directive({
    selector:'[appDropdown]'
})
export class dropdownDirective implements OnInit{
    @HostBinding('class.open') isOpen = false;
    
    constructor(elementRef: ElementRef,private renderer: Renderer2){    }

    @HostListener('click') toggleOpen(){
        this.isOpen = !this.isOpen;
    }

    ngOnInit(){

    }
}
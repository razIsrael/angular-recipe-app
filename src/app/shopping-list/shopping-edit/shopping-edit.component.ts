import { Component, OnInit, Output, ViewChild, ElementRef, EventEmitter, OnDestroy} from '@angular/core';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { shoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  // @ViewChild('nameInput',{static: false}) IngredientNameInput: ElementRef;
  // @ViewChild('amountInput',{static: false}) IngredientAmountInput: ElementRef;
  // @Output() ingredientAdded = new EventEmitter<Ingredient>();
 
  constructor(private slService: shoppingListService) { }
  @ViewChild('f',{static: false}) slForm: NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;

  ngOnInit() {
   this.subscription = this.slService.startedEditing
    .subscribe((index: number) => {
      this.editedItemIndex = index;
      this.editMode = true;
      this.editedItem = this.slService.getIngedient(index);
      this.slForm.setValue({
        name: this.editedItem.name,
        amount: this.editedItem.amount
      })
      }
    );
  }


  onSubmit(form: NgForm){
    // const ingName= this.IngredientNameInput.nativeElement.value;
    // const ingAmount = this.IngredientAmountInput.nativeElement.value;
    const value = form.value;
    const newIngredient = new Ingredient(value.name,value.amount);
    if(this.editMode){
      this.slService.updateIngredient(this.editedItemIndex, newIngredient);
    }else{
      this.slService.addIngredient(newIngredient);
    }
    this.editMode = false;
    form.reset();
    // this.ingredientAdded.emit(newIngredient);

  }
  onClear(){
    this.slForm.reset();
    this.editMode = false;
  }
  onDelete(){
    this.slService.deleteIngredient(this.editedItemIndex);
    this.onClear();
  }

ngOnDestroy(){
  this.subscription.unsubscribe();
}
}

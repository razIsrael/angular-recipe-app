import { Component, OnInit, ViewChild, ElementRef, EventEmitter, OnDestroy } from '@angular/core';

import {Ingredient} from "../shared/ingredient.model";
import { shoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients: Ingredient[];
  private subscription: Subscription;

  constructor( private slService: shoppingListService) { }

  ngOnInit() {
    this.ingredients = this.slService.getIngredient();

    this.subscription = this.slService.ingredientAdded.subscribe(
       (ingredientAdded:Ingredient[]) => { this.ingredients = ingredientAdded; }
     )

  }
  onEditItem(index: number){
    this.slService.startedEditing.next(index);

  }
  // addIngredient(ingredientToAdd: Ingredient){
  // this.ingredients.push(ingredientToAdd);}
  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }
}

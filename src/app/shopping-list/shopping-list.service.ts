import { Ingredient } from '../shared/ingredient.model';
// import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';

export class shoppingListService {
    // ingredientAdded = new EventEmitter<Ingredient[]>();
    ingredientAdded = new Subject<Ingredient[]>();
    startedEditing = new Subject<number>();

    private ingredients: Ingredient[] = [
        new Ingredient("appels",5),
        new Ingredient("banana",10)
      ];
      getIngredient(){
        return this.ingredients.slice();
    }    
    getIngedient(index:number){
      return this.ingredients[index];
    }
      addIngredient(ingredientToAdd: Ingredient){
        this.ingredients.push(ingredientToAdd);
        this.ingredientAdded.next(this.ingredients.slice());
        }
       addIngredients(ingredients: Ingredient[]){
        // for (let ingredient of ingredients){
        //   this.addIngredient(ingredient); }
        this.ingredients.push(...ingredients);
        this.ingredientAdded.next(this.ingredients.slice());
       }
       updateIngredient(index: number, newIngredient: Ingredient){
         this.ingredients[index] = newIngredient;
         this.ingredientAdded.next(this.ingredients.slice());
       }
       deleteIngredient(index:number){
         this.ingredients.splice(index, 1);
         this.ingredientAdded.next(this.ingredients.slice());
       }
}

// ingredientAdded = ingredients changed